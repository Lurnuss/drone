const keysState = {}, upButtonKey = 38, downButtonKey = 40, leftButtonKey = 37, rightButtonKey = 39
const speed = 100

const drone = $('#drone'), tooltip = $('#drone .tooltip')
const screenWidth = $(window).width() - drone.outerWidth() - 20, screenHeight = $(window).height() - drone.innerHeight()

$(document).ready(function () {
  setInterval(updateDronePosition, 100)

  let tooltipTimeout
  drone.click(() => {
    tooltip.show()
    clearTimeout(tooltipTimeout)
    tooltipTimeout = setTimeout(() => tooltip.hide(), 2000)
  })

  $(window).keydown((e) => keysState[e.which] = true)
  $(window).keyup((e) => keysState[e.which] = false)
})

function updateDronePosition() {
  const currentTopPosition = parseInt(drone.css('top'), 10)
  const currentLeftPosition = parseInt(drone.css('left'), 10)

  let top = currentTopPosition, left = currentLeftPosition

  if(keysState[upButtonKey] && !keysState[downButtonKey]) {
    top = currentTopPosition - speed
  } else if(keysState[downButtonKey] && !keysState[upButtonKey]) {
    top = currentTopPosition + speed
  }

  if(keysState[leftButtonKey] && !keysState[rightButtonKey]) {
    left = currentLeftPosition - speed
    drone.removeClass("moving-right")
    drone.addClass("moving-left")
  } else if(keysState[rightButtonKey] && !keysState[leftButtonKey]) {
    left = currentLeftPosition + speed
    drone.removeClass("moving-left")
    drone.addClass("moving-right")
  } else {
    drone.removeClass()
  }

  const leftCoordinate = Math.max(Math.min(left, screenWidth), 0)
  const topCoordinate = Math.max(Math.min(top, screenHeight), 0)

  drone.css({ left: leftCoordinate, top: topCoordinate })
}
